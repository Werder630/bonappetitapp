package com.apps.werder.bonappetit.jsoupclasses;

import android.os.Parcel;
import android.os.Parcelable;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Werder on 01.02.15.
 */
public class MenuListItemJSOUP implements Parcelable {

    private Document doc;

    private List<String> title;
    private List<String> imagelink;
    private List<String> consist;
    private List<String> price;

    MenuListItemJSOUP mli;

    public MenuListItemJSOUP() {}

    public MenuListItemJSOUP(Parcel parcel)
    {
        title = parcel.createStringArrayList();
        imagelink = parcel.createStringArrayList();
        consist = parcel.createStringArrayList();
        price = parcel.createStringArrayList();
    }

    public MenuListItemJSOUP getMenuItem(final String s)
    {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mli = get(s);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        t.start();

        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return mli;
    }

    private MenuListItemJSOUP get(final String secondPartURL) throws IOException
    {
        doc = Jsoup.connect(MenuListJSOUP.URLSITE+""+secondPartURL).get();

        Elements elements = doc.select("div.content.clearfix");

        Elements elements_mi_title = elements.select("h2");
        title = new ArrayList<>();
        for(int i = 0; i < elements_mi_title.size()-1; i++)       {
            title.add(elements_mi_title.get(i).text());
        }

        Elements elements_mi_image = elements.select("img");
        imagelink = new ArrayList<>();
        for(int i = 0; i < getCount(); i++)        {
            imagelink.add(elements_mi_image.get(i).attr("src"));
        }

        Elements elements_mi_consist = elements.select("td");

        consist = new ArrayList<>();
        price = new ArrayList<>();
        for(int i = 0; i < getCount(); i++)       {
            String s = elements_mi_consist.get(i).text();
            if(s.contains("Состав:"))  {
                consist.add(s.substring(s.indexOf("Состав:"), s.indexOf(".")+1));
                String[] temp = s.substring(s.indexOf(".")+1).split("/");
                price.add(temp[0]);}
            else  {
            consist.add("");
            price.add(s);}
        }
        return this;
    }

    public List<String> getTitleList()  {return title;}
    public List<String> getImageList()  {return imagelink;}
    public List<String> getConsistList()  {return consist;}
    public List<String> getPriceList()  {return price;}
    public int getCount() {return getTitleList().size();}


    /**
     * Всякая фигня про Parseble
     * @return
     */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
       dest.writeStringList(title);
       dest.writeStringList(imagelink);
       dest.writeStringList(consist);
       dest.writeStringList(price);
    }

    public static final Parcelable.Creator<MenuListItemJSOUP> CREATOR = new Parcelable.Creator<MenuListItemJSOUP>() {

        @Override
        public MenuListItemJSOUP createFromParcel(Parcel source) {

            return new MenuListItemJSOUP(source);
        }

        @Override
        public MenuListItemJSOUP[] newArray(int size) {
            return new MenuListItemJSOUP[0];
        }

    };
}
