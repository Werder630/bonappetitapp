package com.apps.werder.bonappetit.activity;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.apps.werder.bonappetit.InformationAboutFailInternet;
import com.apps.werder.bonappetit.ProblemInternetDialogCloseListener;
import com.apps.werder.bonappetit.R;
import com.apps.werder.bonappetit.adapters.MyExpListAdapter;
import com.apps.werder.bonappetit.jsoupclasses.MenuListItemJSOUP;
import com.apps.werder.bonappetit.jsoupclasses.MenuListJSOUP;

import java.util.ArrayList;


public class MenuListActivity extends ActionBarActivity implements ProblemInternetDialogCloseListener {

    ExpandableListView elv;
    MyExpListAdapter myExpAdap;
    ArrayList<MenuListJSOUP> menulist;
    InformationAboutFailInternet inf;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_list_view);

        ActionBar ab = getSupportActionBar();
        ab.setTitle("Меню");
        ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color_dark)));
        ab.setDisplayShowHomeEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setIcon(R.drawable.bon_appetit_logo_actionbar);

        inf = new InformationAboutFailInternet(this);
        elv = (ExpandableListView) findViewById(R.id.menu_lv);
        menulist = new ArrayList<>();

        pd = new ProgressDialog(this);
        pd.setIndeterminate(true);
        pd.setMessage("Идет загрузка...");
        pd.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                init();
                Looper.loop();
            }
        }).start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init()
    {
        if (inf.checkInternet()) {
            try {
                menulist.add(new MenuListJSOUP("block-menu-menu-european-menu").execute().get());
                menulist.add(new MenuListJSOUP("block-menu-menu-japanese-menu").execute().get());
                myExpAdap = new MyExpListAdapter(this.getApplicationContext(), menulist);
            } catch (Exception e) {
                e.printStackTrace();
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    elv.setAdapter(myExpAdap);
                    elv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                        @Override
                        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                            Intent intent = new Intent(MenuListActivity.this, FoodActivity.class);
                            intent.putExtra("listmenu", new MenuListItemJSOUP().getMenuItem(menulist.get(groupPosition).getLink().get(childPosition)));
                            intent.putExtra("title", ((TextView) v.findViewById(R.id.food_text)).getText());
                            MenuListActivity.this.startActivity(intent);
                            return true;
                        }
                    });
                    pd.dismiss();
                }
            });
        } else
        {
            inf.showFialInternetDialod();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void closeDialog() {
        init();
    }
}
