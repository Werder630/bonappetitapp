package com.apps.werder.bonappetit.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.werder.bonappetit.Constants;
import com.apps.werder.bonappetit.Order;
import com.apps.werder.bonappetit.R;
import com.apps.werder.bonappetit.contacts.ContactsActivity;


public class OrderActivity extends ActionBarActivity {

    TextView generalInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        ActionBar ab = getSupportActionBar();

        ab.setTitle("Мой заказ");
        ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color_dark)));

        ab.setDisplayShowHomeEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setIcon(R.drawable.bon_appetit_logo_actionbar);

        generalInfo = ((TextView)findViewById(R.id.activity_order_generalinfo));
        generalInfo.setTypeface(Constants.getFont(this));
        updateInfoText();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void updateInfoText() {
        if (Order.getTotalSum() == 0) generalInfo.setVisibility(View.GONE);
        else {
            if (generalInfo.getVisibility() == View.GONE) {generalInfo.setVisibility(View.VISIBLE);}
            generalInfo.setText("Общий заказ на " + Order.getTotalSum() + " руб.");
        }
    }

    public void sendOrder(View v) {
        if (!this.getSharedPreferences("mydata", Context.MODE_PRIVATE).getBoolean(MyDataActivity.ISFILL, false)) {
            Intent intent = new Intent(this, MyDataActivity.class);
            intent.putExtra("isorder", "Отправить");
            startActivity(intent);
        } else {
            AlertDialog ad = new AlertDialog.Builder(this)
                .setItems(new String[] {"Использовать мои данные", "Указать новые данные"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                         switch(which) {
                             case 0:
                                 Toast.makeText(OrderActivity.this, "Заказ отправлен", Toast.LENGTH_LONG).show();
                                 break;
                             case 1:
                                 Intent intent = new Intent(OrderActivity.this, MyDataActivity.class);
                                 intent.putExtra("isorder", true);
                                 startActivity(intent);
                                 break;
                         }
                    }
                })
                .setTitle("Выберите способ доставки")
                .create();
           ad.show();
       }
    }
}
