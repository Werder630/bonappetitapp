package com.apps.werder.bonappetit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Юлия on 16.02.2015.
 */
public class InformationAboutFailInternet {

    Activity activity;
    ProblemInternetDialogCloseListener pidcl;

    public InformationAboutFailInternet(Activity act)
    {
        this.activity = act;
        pidcl = (ProblemInternetDialogCloseListener)act;
    }

    public boolean checkInternet()
    {

        ConnectivityManager cm = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        return (ni != null) && (ni.isConnectedOrConnecting());
    }

    public void showFialInternetDialod()
    {
        new AlertDialog.Builder(activity).setMessage("Проблемы с интернетом").
                setPositiveButton("Ок", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        pidcl.closeDialog();
                    }
                }).
                create().
                show();
    }
}
