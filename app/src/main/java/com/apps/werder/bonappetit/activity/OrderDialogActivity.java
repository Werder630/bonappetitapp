package com.apps.werder.bonappetit.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.apps.werder.bonappetit.Order;
import com.apps.werder.bonappetit.R;


public class OrderDialogActivity extends Activity {

    private int count = 1;
    private int price;
    TextView titleTV, countTV, priceTV;
    String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_order_dialog_create);

        countTV = (TextView)findViewById(R.id.activity_order_dialog_create_textview_count);
        priceTV = (TextView)findViewById(R.id.activity_order_dialog_create_textview_price);
        price = getIntent().getIntExtra("price", 0);
        title = getIntent().getStringExtra("title");
        titleTV = (TextView)findViewById(R.id.activity_order_dialog_create_textview_title);
        titleTV.setText(title);

        updateData();

        findViewById(R.id.activity_order_dialog_create_imageview_minus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if (count > 1) {
                     countTV.setText(--count+"");
                     updateData();
                 }
            }
        });

        findViewById(R.id.activity_order_dialog_create_imageview_plus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countTV.setText(++count+"");
                updateData();
            }
        });

        findViewById(R.id.activity_order_dialog_create_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Order.addOrder(title, price, count);
               finish();
            }
        });
    }

    void updateData() {
        countTV.setText(""+count);
        priceTV.setText(count*price+" руб.");
    }


}
