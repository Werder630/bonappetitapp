package com.apps.werder.bonappetit.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apps.werder.bonappetit.Order;
import com.apps.werder.bonappetit.OrderListFragment;
import com.apps.werder.bonappetit.R;

import java.util.List;

/**
 * Created by Юлия on 04.04.2015.
 */
public class OrdersArrayAdapter<T> extends ArrayAdapter<Order> {
    Context context;
    List<Order> orders;
    OrderListFragment orderListFragment;

    public OrdersArrayAdapter(Context context, OrderListFragment olf, List<Order> list) {
        super(context, R.layout.activity_order_row, list);
        this.context = context;
        this.orders = list;
        this.orderListFragment = olf;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = ((Activity)context).getLayoutInflater().inflate(R.layout.activity_order_row, null, true);
            holder = new ViewHolder();

            holder.VHtitle = (TextView) convertView.findViewById(R.id.activity_order_row_title);
            holder.VHsum = (TextView) convertView.findViewById(R.id.activity_order_row_count);
            holder.VHremove = convertView.findViewById(R.id.activity_order_row_removeitem);
            holder.VHminus = convertView.findViewById(R.id.activity_order_row_minus);
            holder.VHplus = convertView.findViewById(R.id.activity_order_row_plus);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final int pos = position;

        holder.VHremove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderListFragment.showDeletePositionDialog(pos);
            }
        });

        holder.VHminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orders.get(position).getCount() > 1) {
                orders.get(position).setCount(orders.get(position).getCount()-1);
                orderListFragment.updateAdapter();  }
            }
        });

        holder.VHplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orders.get(position).setCount(orders.get(position).getCount()+1);
                orderListFragment.updateAdapter();
            }
        });

        holder.VHtitle.setText(orders.get(position).getTitle());
        holder.VHsum.setText(orders.get(position).getCount()+" / "+orders.get(position).getSum()+" р.");

        return convertView;
    }

    static class ViewHolder {
        public TextView VHtitle;
        public TextView VHsum;
        public View VHremove;
        public View VHminus;
        public View VHplus;
    }
}
