package com.apps.werder.bonappetit;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;

import com.apps.werder.bonappetit.activity.OrderActivity;
import com.apps.werder.bonappetit.adapters.OrdersArrayAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class OrderListFragment extends ListFragment {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateAdapter();
        getListView().setDivider(new ColorDrawable(R.color.main_color_dark));
        getListView().setDividerHeight(3);
        this.setEmptyText("Заказ не выбран");
    }

    public void updateAdapter() {
        ArrayAdapter<Order> adapter = new OrdersArrayAdapter<String>(getActivity(), this, Order.orders);
        setListAdapter(adapter);
        ((OrderActivity)getActivity()).updateInfoText();
    }

    public void showDeletePositionDialog(final int position) {
        AlertDialog ad = new AlertDialog.Builder(getActivity())
                .setTitle("Подтверждение удаления")
                .setMessage("Вы действительно хотите удалить позицию " + Order.orders.get(position).getTitle() + " из заказа")
                .setPositiveButton("Удалить", new Dialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Order.deleteFromOrder(position);
                        updateAdapter();
                    }
                })
                .setNegativeButton("Отмена", new Dialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create();

        ad.show();
    }

}
