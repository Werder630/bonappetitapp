package com.apps.werder.bonappetit.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.werder.bonappetit.R;

public class MyDataActivity extends ActionBarActivity {

    EditText name, phone, street, house, access, floor, flat;
    public static final String ISFILL = "i", NAME = "n", PHONE = "p", STREET = "s", HOUSE = "h", ACCESS = "a", FLOOR = "floor", FLAT = "flat";
    boolean isOrder = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mydata);

        ActionBar ab = getSupportActionBar();
        ab.setTitle("Данные для доставки");
        ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color_dark)));

        ab.setDisplayShowHomeEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setIcon(R.drawable.bon_appetit_logo_actionbar);

        name = (EditText)findViewById(R.id.mydata_edittext_name);
        phone = (EditText)findViewById(R.id.mydata_edittext_phone);
        street = (EditText)findViewById(R.id.mydata_edittext_street);
        house = (EditText)findViewById(R.id.mydata_edittext_house);
        access = (EditText)findViewById(R.id.mydata_edittext_access);
        floor = (EditText)findViewById(R.id.mydata_edittext_floor);
        flat = (EditText)findViewById(R.id.mydata_edittext_flat);

        isOrder = getIntent().getBooleanExtra("isorder", false);

        ((TextView)findViewById(R.id.mydata_button_save)).setText(isOrder ? "Отправить" : "Сохранить");

        init();
    }

    private void init() {
        SharedPreferences sp = this.getSharedPreferences("mydata", Context.MODE_PRIVATE);
        name.setText(sp.getString(NAME, ""));
        phone.setText(sp.getString(PHONE, ""));
        street.setText(sp.getString(STREET, ""));
        house.setText(sp.getString(HOUSE, ""));
        access.setText(sp.getString(ACCESS, ""));
        floor.setText(sp.getString(FLOOR, ""));
        flat.setText(sp.getString(FLAT, ""));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_my_data, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.clear_mydata:
                clearMyData();
            default:
                return super.onOptionsItemSelected(item);
        }}

    private void clearMyData() {
        SharedPreferences sp = this.getSharedPreferences("mydata", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.putString(NAME, "");   name.setText("");
        editor.putString(PHONE, "");  phone.setText("");
        editor.putString(STREET, ""); street.setText("");
        editor.putString(HOUSE, "");  house.setText("");
        editor.putString(ACCESS, ""); access.setText("");
        editor.putString(FLOOR, "");  floor.setText("");
        editor.putString(FLAT, "");   flat.setText("");
        editor.putBoolean(ISFILL, false);

        editor.apply();

    }

    public void saveData(View v) {
        if (phone.getText().toString().isEmpty()) {Toast.makeText(this, "Укажите номер телефона", Toast.LENGTH_SHORT).show(); phone.requestFocus();}
        else if (street.getText().toString().isEmpty()) {Toast.makeText(this, "Укажите улицу", Toast.LENGTH_SHORT).show(); street.requestFocus();}
        else if (house.getText().toString().isEmpty()) {Toast.makeText(this, "Укажите номер дома", Toast.LENGTH_SHORT).show(); house.requestFocus();}
        else if (flat.getText().toString().isEmpty()) {Toast.makeText(this, "Укажите номер квартиры", Toast.LENGTH_SHORT).show(); flat.requestFocus();}
        else {
            SharedPreferences sp = this.getSharedPreferences("mydata", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();

            editor.putString(NAME, name.getText().toString());
            editor.putString(PHONE, phone.getText().toString());
            editor.putString(STREET, street.getText().toString());
            editor.putString(HOUSE, house.getText().toString());
            editor.putString(ACCESS, access.getText().toString());
            editor.putString(FLOOR, floor.getText().toString());
            editor.putString(FLAT, flat.getText().toString());
            editor.putBoolean(ISFILL, true);

            editor.apply();

            Toast.makeText(this, isOrder ? "Данные успешно сохранены" : "Заказ отправлен", Toast.LENGTH_SHORT).show();
        }
    }
}
