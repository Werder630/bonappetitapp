package com.apps.werder.bonappetit.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.werder.bonappetit.Constants;
import com.apps.werder.bonappetit.activity.OrderDialogActivity;
import com.apps.werder.bonappetit.R;
import com.apps.werder.bonappetit.jsoupclasses.MenuListItemJSOUP;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Werder on 01.02.15.
 */
public class MyFoodAdapter extends ArrayAdapter {

    Context context;
    List<Bitmap> bitmapList;
    MenuListItemJSOUP menulistitem;
    int w;
    DisplayImageOptions options;

    public MyFoodAdapter(Context con, MenuListItemJSOUP mli)
    {   super(con, R.layout.activity_food_item, mli.getTitleList());
        this.context = con;
        this.menulistitem = mli;

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        w = displaymetrics.widthPixels;
        options = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .showImageOnLoading(R.drawable.bon_appetit_logo)
                .preProcessor(new BitmapProcessor() {
                    @Override
                    public Bitmap process(Bitmap bmp) {
                        return Bitmap.createScaledBitmap(bmp, w*3/10, w/4, false);
                    }
                })
                .build();
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));

        bitmapList = new ArrayList<>();
        for (final String url : menulistitem.getImageList()) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    bitmapList.add(ImageLoader.getInstance().loadImageSync(url, options));
                }
            });
            t.start();

            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        View rowview = convertView;

        if (rowview == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = inflater.inflate(R.layout.activity_food_item, null);

            holder = new ViewHolder();

            holder.title = (TextView)rowview.findViewById(R.id.food_item_title);
            holder.title.setTypeface(Constants.getFont(context));
            holder.consist = (TextView)rowview.findViewById(R.id.food_item_consist);
            holder.price = (TextView)rowview.findViewById(R.id.food_item_price);
            holder.icon = (ImageView)rowview.findViewById(R.id.food_item_image);
            holder.button = (Button)rowview.findViewById(R.id.food_item_button_create_order);

            rowview.setTag(holder);
        }

        else {
            holder = (ViewHolder) rowview.getTag();
        }

        holder.title.setText(menulistitem.getTitleList().get(position));
        final int i = position;
        holder.consist.setText(menulistitem.getConsistList().get(position));
        holder.price.setText(menulistitem.getPriceList().get(position));
        holder.icon.setImageBitmap(bitmapList.get(position));
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCreateOrderDialog(menulistitem.getTitleList().get(i), getPriceFromString(menulistitem.getPriceList().get(position)));
            }
        });

        return rowview;
    }

    private int getPriceFromString(String s) {
        if (s.contains("р.")) {
            int last = s.indexOf("р.")-1;
            for(int i = last; i > 0; i--)  {
                if(s.substring(i-1, i).equals(" ")) {
                    String result = s.substring(i, last).trim();
                    Log.i("test", result);
                    try {return Integer.parseInt(result);}
                    catch (NumberFormatException nfe) {return 300;}
                }
            }
        }
        return 300;
    }

    void showCreateOrderDialog(String t, int p)  {
        Intent intent = new Intent(context, OrderDialogActivity.class);
        intent.putExtra("title", t);
        intent.putExtra("price", p);
        ((Activity)context).startActivityForResult(intent, 1);
    }



    static class ViewHolder {
        Button button;
        TextView title, consist, price;
        ImageView icon;
    }

}
