package com.apps.werder.bonappetit.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.apps.werder.bonappetit.Constants;
import com.apps.werder.bonappetit.R;
import com.apps.werder.bonappetit.jsoupclasses.MenuListJSOUP;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Werder on 31.01.15.
 */
public class MyExpListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<MenuListJSOUP> group;

    public MyExpListAdapter(Context context, ArrayList<MenuListJSOUP> groups)
    {
       this.context = context;
       this.group = groups;
    }

    @Override
    public int getGroupCount() {
        return group.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return group.get(groupPosition).getFood().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return group.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return group.get(groupPosition).getFood().get(childPosition);}

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.menu_kitchen, null);
        }

        TextView textGroup = (TextView) convertView.findViewById(R.id.kitchen_text);
        textGroup.setText(group.get(groupPosition).getKitchen());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.menu_food, null);
        }

        TextView textChild = (TextView) convertView.findViewById(R.id.food_text);
        textChild.setTypeface(Constants.getFont(context));
        textChild.setText(group.get(groupPosition).getFood().get(childPosition));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
