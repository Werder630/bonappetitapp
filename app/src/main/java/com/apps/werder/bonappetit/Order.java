package com.apps.werder.bonappetit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Юлия on 04.04.2015.
 */
public class Order {
    public static List<Order> orders = new ArrayList<>();

    private String name;
    private int price;
    private int count;

    private Order(String n, int p, int c) {
        this.name = n;
        this.price = p;
        this.count = c;

        orders.add(this);
    }

    public static void addOrder(String n, int p, int c) {
        new Order(n, p, c);
    }

    public String getTitle() {
        return name;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public int getSum() {
        return count*price;
    }

    public static int getTotalSum() {
        int result = 0;
        for (Order o : orders) {
            result += o.price*o.count;
        }
        return result;
    }

    public static void deleteFromOrder(int positionForDelete) {
            orders.remove(positionForDelete);
    }


}
