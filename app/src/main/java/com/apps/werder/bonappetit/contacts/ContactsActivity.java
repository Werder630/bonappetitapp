package com.apps.werder.bonappetit.contacts;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps.werder.bonappetit.R;
import com.google.android.gms.common.GooglePlayServicesUtil;


public class ContactsActivity extends ActionBarActivity implements View.OnClickListener{

    LinearLayout ll;
    TextView title, address, phone_cafe, phone_delivery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        ActionBar ab = getSupportActionBar();
        ab.setTitle("Контакты");

        ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color_dark)));

        ab.setDisplayShowHomeEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setIcon(R.drawable.bon_appetit_logo_actionbar);

        if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) != 0)
        {findViewById(R.id.contacts_button_map).setVisibility(View.GONE);}

        ll = (LinearLayout)findViewById(R.id.activity_contacts_ll);
        LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view1 = inflater.inflate(R.layout.contacts_item, null);

        title = (TextView)view1.findViewById(R.id.contacts_item_textview_title);
        title.setText(getResources().getString(R.string.contacts_title_1));
        address = (TextView)view1.findViewById(R.id.contacts_item_textview_address);
        address.setText(getResources().getString(R.string.contacts_address_1));
        phone_cafe = (TextView)view1.findViewById(R.id.contacts_item_textview_phonecafe);
        phone_cafe.setText("(4912) "+getResources().getString(R.string.contacts_phonecafe_1));
        phone_cafe.setOnClickListener(this);
        phone_delivery = (TextView)view1.findViewById(R.id.contacts_item_textview_phonedelivery);
        phone_delivery.setText("(4912) "+getResources().getString(R.string.contacts_phonedelivery_1));
        phone_delivery.setOnClickListener(this);

        ll.addView(view1);

        View view2 = inflater.inflate(R.layout.contacts_item, null);

        title = (TextView)view1.findViewById(R.id.contacts_item_textview_title);
        title.setText(getResources().getString(R.string.contacts_title_2));
        address = (TextView)view2.findViewById(R.id.contacts_item_textview_address);
        address.setText(getResources().getString(R.string.contacts_address_2));
        phone_cafe = (TextView)view2.findViewById(R.id.contacts_item_textview_phonecafe);
        phone_cafe.setText("(4912) "+getResources().getString(R.string.contacts_phonecafe_2));
        phone_cafe.setOnClickListener(this);
        phone_delivery = (TextView)view2.findViewById(R.id.contacts_item_textview_phonedelivery);
        phone_delivery.setText("(4912) "+getResources().getString(R.string.contacts_phonedelivery_2));
        phone_delivery.setOnClickListener(this);

        ll.addView(view2);

        findViewById(R.id.contacts_button_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactsActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + ((TextView)v).getText().subSequence(6, 14)));
        this.startActivity(intent);
    }
}
