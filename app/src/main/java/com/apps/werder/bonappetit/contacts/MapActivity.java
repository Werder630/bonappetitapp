package com.apps.werder.bonappetit.contacts;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.apps.werder.bonappetit.R;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Werder on 13.03.15.
 */
public class MapActivity extends FragmentActivity {

    SupportMapFragment mapFragment;
    GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_map);

        Log.i("test", "onCreate in fragment");

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        map = mapFragment.getMap();
        if (map == null) {
            finish();
            return;
        }

        Log.i("test", "" + GooglePlayServicesUtil.isGooglePlayServicesAvailable(this));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(54.62604638391604, 39.751150942016544))
                .zoom(12)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        map.animateCamera(cameraUpdate);

        map.addMarker(new MarkerOptions().position(new LatLng(54.631504, 39.774445))
                .title("Кафе Бон Аппетит на Солотчинском шоссе"));

        map.addMarker(new MarkerOptions().position(new LatLng(54.613682, 39.722226))
                .title("Кафе Бон Аппетит на Гагарина"));
    }
}
