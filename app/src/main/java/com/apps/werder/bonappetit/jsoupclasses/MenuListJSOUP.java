package com.apps.werder.bonappetit.jsoupclasses;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Werder on 31.01.15.
 */
public class MenuListJSOUP extends AsyncTask<Void, Void, MenuListJSOUP>{

    final static String URLSITE = "http://bonappetit-62.ru";
    final static String URL = URLSITE+"/menu/pizza.html";
    Document doc;
    private String valueOfDiv;
    private String kitchen;
    private ArrayList<String> stringList;
    private ArrayList<String> linkList;

    public MenuListJSOUP(String s)
    {this.valueOfDiv = s;
     }

    @Override
    protected MenuListJSOUP doInBackground(Void... params) {
        getMenu(valueOfDiv);
        return this;
    }

    void getMenu(String s)
    {
        try {
        doc = Jsoup.connect(URL).get();
    } catch (IOException e) {
        e.printStackTrace();
    }
        Elements elements = doc.select("div#" + s);
        kitchen = elements.select("h2").text();
        Elements food =  elements.select("li");
        stringList = new ArrayList<>();
        Elements link =  elements.select("a");
        linkList = new ArrayList<>();

        for(int i = 0; i < food.size(); i++)
        {
            stringList.add(food.get(i).text());
            linkList.add(link.get(i).attr("href"));
        }
    }

    public String getKitchen()
    {
        return kitchen;
    }

    public List<String> getFood()
    {
        return stringList;
    }

    public List<String> getLink()
    {
        return linkList;
    }


}
