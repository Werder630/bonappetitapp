package com.apps.werder.bonappetit.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.werder.bonappetit.Order;
import com.apps.werder.bonappetit.R;
import com.apps.werder.bonappetit.adapters.MyFoodAdapter;
import com.apps.werder.bonappetit.jsoupclasses.MenuListItemJSOUP;

/**
 * Created by Werder on 01.02.15.
 */
public class FoodActivity extends ActionBarActivity {

    ListView lv;
    TextView tv;
    MenuListItemJSOUP mli;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_item_layout);

        mli = this.getIntent().getParcelableExtra("listmenu");
        lv = (ListView)findViewById(R.id.activity_fooditem_layout);

        ActionBar ab = getSupportActionBar();
        ab.setTitle(this.getIntent().getStringExtra("title"));
        ab.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.main_color_dark)));
        ab.setDisplayShowHomeEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setIcon(R.drawable.bon_appetit_logo_actionbar);

        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.actionbar_menuitem_orders, null);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Order.orders.size() > 0) {
                   Intent intent = new Intent(FoodActivity.this, OrderActivity.class);

                   startActivity(intent); }
                else {
                   Toast.makeText(FoodActivity.this, "Сначала что нибудь закажи!", Toast.LENGTH_LONG).show();
                }
            }
        });

        tv = (TextView)v.findViewById(R.id.actionbar_menuitem_orders_textview_price);

        getSupportActionBar().setCustomView(v, lp);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        pd = new ProgressDialog(this);
        pd.setIndeterminate(true);
        pd.setMessage("Идет загрузка...");
        pd.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                final MyFoodAdapter mfa = new MyFoodAdapter(FoodActivity.this, mli);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lv.setAdapter(mfa);
                        lv.setDivider(null);
                        pd.dismiss();
                    }
                });
                Looper.loop();
            }
        }).start();

        tv.setText(Order.getTotalSum()+" руб.");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        tv.setText(Order.getTotalSum()+" руб.");
    }



}
