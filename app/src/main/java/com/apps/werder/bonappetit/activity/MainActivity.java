package com.apps.werder.bonappetit.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.apps.werder.bonappetit.Constants;
import com.apps.werder.bonappetit.R;
import com.apps.werder.bonappetit.contacts.ContactsActivity;

/**
 * Created by Werder on 07.03.15.
 */
public class MainActivity extends Activity implements View.OnClickListener{

    int height, width;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.activity_main_button_menu).setOnClickListener(this);
        findViewById(R.id.activity_main_button_delivery).setOnClickListener(this);
        findViewById(R.id.activity_main_button_contacts).setOnClickListener(this);
        findViewById(R.id.activity_main_button_mydata).setOnClickListener(this);

        findViewById(R.id.main_image_vk).setOnClickListener(this);
        findViewById(R.id.main_image_ok).setOnClickListener(this);
        findViewById(R.id.main_image_inst).setOnClickListener(this);

        ((TextView)findViewById(R.id.activity_main_button_menu)).setTypeface(Constants.getFont(this));
        ((TextView)findViewById(R.id.activity_main_button_delivery)).setTypeface(Constants.getFont(this));
        ((TextView)findViewById(R.id.activity_main_button_contacts)).setTypeface(Constants.getFont(this));
        ((TextView)findViewById(R.id.activity_main_button_mydata)).setTypeface(Constants.getFont(this));

    }

    @Override
    public void onClick(View v) {
        switch(v.getId())  {
            case R.id.activity_main_button_menu:     {  openActivity(MenuListActivity.class); break;}
            case R.id.activity_main_button_delivery: {  openActivity(OrderActivity.class); break;}
            case R.id.activity_main_button_contacts: {  openActivity(ContactsActivity.class); break;}
            case R.id.activity_main_button_mydata:   {  openActivity(MyDataActivity.class); break;}
            case R.id.main_image_vk:                 {  openLink(getString(R.string.main_link_vk)); break;}
            case R.id.main_image_ok:                 {  openLink(getString(R.string.main_link_ok)); break;}
            case R.id.main_image_inst:               {  openLink(getString(R.string.main_link_inst)); break;}
            default: break;
        }
    }


    void openActivity(Class intentClass)  {
        Intent intent = new Intent(this, intentClass);
        startActivity(intent);
    }

    void openLink(String url)  {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }
}
