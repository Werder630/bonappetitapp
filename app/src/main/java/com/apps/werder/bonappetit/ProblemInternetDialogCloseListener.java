package com.apps.werder.bonappetit;

/**
 * Created by Юлия on 16.02.2015.
 */
public interface ProblemInternetDialogCloseListener {

    void closeDialog();
}
