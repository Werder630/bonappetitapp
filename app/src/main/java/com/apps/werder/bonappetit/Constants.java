package com.apps.werder.bonappetit;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Юлия on 08.05.2015.
 */
public class Constants {

    static public Typeface getFont(Context context) {
        return  Typeface.createFromAsset(context.getAssets(), "fonts/ComicRelief.ttf");
    }

}
